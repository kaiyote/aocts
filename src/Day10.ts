import { IDay } from './IDay'

export default class Day10 implements IDay<number | string> {
  ringLength: number = 0
  part1 (input: string): number {
    let position = 0
    let skipSize = 0
    const ring = this.ringProxy(Array.apply(null, { length: this.ringLength }).map(Number.call, Number))
    const lengths = this.prepareInput(input)

    for (const length of lengths) {
      const toFlip = []
      for (let i = position; i < position + length; i++) toFlip.push(ring[i])
      for (const flipped of toFlip.reverse()) ring[position++] = flipped
      position += skipSize++
    }

    return ring[0] * ring[1]
  }

  part2 (input: string): string {
    let position = 0
    let skipSize = 0
    this.ringLength = 256
    const ring = this.ringProxy(Array.apply(null, { length: this.ringLength }).map(Number.call, Number))
    const lengths = this.prepareInput2(input)

    for (let j = 0; j < 64; j++) {
      for (const length of lengths) {
        const toFlip = []
        for (let i = position; i < position + length; i++) toFlip.push(ring[i])
        for (const flipped of toFlip.reverse()) ring[position++] = flipped
        position += skipSize++
      }
    }

    const sparseHash = []
    for (let i = 0; i < 16; i++) {
      sparseHash.push(ring.slice(i * 16, i * 16 + 16).reduce((prev, curr) => prev ^ curr))
    }

    return sparseHash.map(x => x.toString(16).padStart(2, '0')).join('').toLowerCase()
  }

  private prepareInput (input: string): number[] {
    return input.split(',').map(x => +x.trim()).filter(x => x <= this.ringLength)
  }

  private prepareInput2 (input: string): number[] {
    return input.trim().split('').map(x => x.charCodeAt(0)).concat(17,31, 73, 47, 23)
  }

  private ringProxy (arr: number[]): number[] {
    return new Proxy(arr, {
      get: (target, name) => {
        if (name in target) {
          const func = new Function('arr', `return arr['${String(name)}']`)
          return func(target)
        } else if (typeof name !== 'symbol') {
          return target[+name % target.length]
        }
      },
      set: (target, name, value) => {
        if (name in target) {
          const func = new Function('arr, value', `arr['${String(name)}'] = value; return true`)
          return func(target, value)
        } else if (typeof name !== 'symbol') {
          target[+name % target.length] = value
          return true
        }
      }
    })
  }
}
