import { IDay } from './IDay'

export default class Day5 implements IDay<number> {
  part1 (input: string): number {
    let idx = 0
    let jumpCounter = 0
    const data = this.prepareInput(input)
    while (idx < data.length && idx >= 0) {
      const newIdx = idx + data[idx]
      data[idx] += 1
      idx = newIdx
      jumpCounter++
    }

    return jumpCounter
  }

  part2 (input: string): number {
    let idx = 0
    let jumpCounter = 0
    const data = this.prepareInput(input)
    while (idx < data.length && idx >= 0) {
      const newIdx = idx + data[idx]
      data[idx] >= 3 ? data[idx] -= 1 : data[idx] += 1
      idx = newIdx
      jumpCounter++
    }

    return jumpCounter
  }

  private prepareInput (input: string): number[] {
    return input.split('\n').map(x => +x.trim())
  }
}
