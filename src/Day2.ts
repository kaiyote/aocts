import { IDay } from './IDay'

export default class Day2 implements IDay<number> {
  part1 (input: string): number {
    return this.prepareInput(input)
      .map(x => x.sort((a, b) => a - b))
      .map(x => [-x[0], x[x.length - 1]])
      .map(x => x[0] + x[1])
      .reduce((prev, curr) => prev + curr)
  }

  part2 (input: string): number {
    return this.prepareInput(input)
      .map(this.findDivisors)
      .filter((x: number | void): x is number => typeof x === 'number')
      .reduce((prev, curr) => prev + curr)
  }

  private prepareInput (input: string): number[][] {
    return input.split('\n').map(x => x.trim().split(/\s+/g).map(y => +y))
  }

  private findDivisors (row: number[]): number | void {
    for (const x of row) {
      for (const y of row.filter(z => z !== x)) {
        if (x / y === ((x / y) | 0)) return x / y
      }
    }
  }
}
