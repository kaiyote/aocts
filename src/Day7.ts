import { IDay } from './IDay'

export default class Day7 implements IDay<string | number> {
  part1 (input: string): string {
    const programs = this.prepareInput(input)

    return programs.find(x => x.parent === undefined)!.name
  }

  part2 (input: string): number {
    const programs = this.prepareInput(input)
    const root = programs.find(x => x.parent === undefined)!
    let unbalanced = this.findUnbalanced(root, programs)!
    let found = false
    while (!found) {
      const unbalancedChild = this.findUnbalanced(unbalanced, programs)
      if (unbalancedChild) unbalanced = unbalancedChild
      else found = true
    }

    const weights = unbalanced.parent!.children
      .map(x => ({ name: x, weight: this.towerWeight(programs.find(y => y.name === x)!, programs) }))

    const diff = weights.find(x => x.name === unbalanced.name)!.weight -
      weights.find(x => x.name !== unbalanced.name)!.weight

    return programs.find(x => x.name === unbalanced.name)!.weight - diff
  }

  private prepareInput (input: string): ITree[] {
    const nameWeightChildren = input.split('\n').map(x => x.trim().split('->').map(y => y.trim()))
    const fullSplit = nameWeightChildren.map(x => x[0].split(' ').concat(x[1] ? x[1].split(', ') : []))
    const programs = fullSplit.map(x => ({
      name: x[0],
      weight: +x[1].replace(')', '').replace('(', ''),
      children: x.slice(2)
    })) as ITree[]

    for (const prog of programs) {
      programs.filter(x => prog.children.includes(x.name)).map(x => x.parent = prog)
    }

    return programs
  }

  private towerWeight (tree: ITree, programs: ITree[]): number {
    let weight = tree.weight
    for (const sub of tree.children) {
      weight += this.towerWeight(programs.find(x => x.name === sub)!, programs)
    }
    return weight
  }

  private findUnbalanced (tree: ITree, programs: ITree[]): ITree | undefined {
    const weights = programs.filter(x => tree.children.includes(x.name))
      .map(x => ({ name: x.name, weight: this.towerWeight(x, programs), count: 1 })) as IWeight[]

    weights.map(x => x.count = weights.filter(y => y.weight === x.weight).length)

    return programs
      .find(y => weights.find(x => x.count === 1) ? weights.find(x => x.count === 1)!.name === y.name : false)
  }
}

interface ITree {
  name: string
  weight: number
  children: string[]
  parent?: ITree
}

interface IWeight {
  name: string
  weight: number
  count: number
}
