import { IDay } from './IDay'

export default class Day6 implements IDay<number> {
  part1 (input: string): number {
    const seen = new Set<string>()
    const data = this.prepareInput(input)
    while (!seen.has(data.join(''))) {
      seen.add(data.join(''))
      this.permute(data)
    }
    return seen.size
  }

  part2 (input: string): number {
    const seen = new Set<string>()
    const data = this.prepareInput(input)
    while (!seen.has(data.join(''))) {
      seen.add(data.join(''))
      this.permute(data)
    }
    return seen.size - Array.from(seen.values()).findIndex(x => x === data.join(''))
  }

  private prepareInput (input: string): number[] {
    return input.split(/\s+/).map(x => +x.trim())
  }

  private permute (data: number[]): void {
    let val = Math.max(...data)
    let idx = data.findIndex(x => x === val)
    data[idx] = 0
    while (val > 0) {
      if (idx === data.length - 1) idx = -1
      data[++idx] += 1
      val -= 1
    }
  }
}
