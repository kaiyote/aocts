import { IDay } from './IDay'

export default class Day8 implements IDay<number> {
  private registers?: { [index: string]: number | undefined }
  part1 (input: string): number {
    this.registers = {}
    const instructions = this.prepareInput(input)
    for (const instruction of instructions) {
      this.execute(instruction)
    }
    return Math.max(...(Object.values(this.registers).filter(x => x !== undefined) as number[]))
  }

  part2 (input: string): number {
    this.registers = {}
    let max = 0
    const instructions = this.prepareInput(input)
    for (const instruction of instructions) {
      this.execute(instruction)
      max = Math.max(max, ...(Object.values(this.registers).filter(x => x !== undefined) as number[]))
    }
    return max
  }

  private prepareInput (input: string): IInstruction[] {
    return input.split('\n').map(x => x.trim().split(' '))
      .map(x => ({
        func: `if ((this.registers.${x[4]} || 0) ${x[5]} ${x[6]}) {
  this.registers.${x[0]} = (this.registers.${x[0]} || 0) ${(x[1] === 'inc' ? '+' : '-')} ${x[2]}
}`
      })) as IInstruction[]
  }

  private execute (instruction: IInstruction): void {
    this.evalInThis(instruction.func)
  }

  private evalInThis (func: string): void {
    // tslint:disable-next-line:no-eval
    (() => eval(func)).call(this)
  }
}

interface IInstruction {
  func: string
}
