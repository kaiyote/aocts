import { IDay } from './IDay'

export default class Day1 implements IDay<number> {
  part1 (input: string): number {
    let data = input.split('')
    data = data.concat(data[0])
    let total = 0
    for (let i = 0; i < data.length - 1; i += 1 | 0) {
      if (data[i] === data[i + 1]) total += +data[i]
    }
    return total
  }

  part2 (input: string): number {
    const data = input.split('')
    let data2 = new Array(...data)
    const frontHalf = data2.splice(0, data2.length / 2)
    data2 = data2.concat(...frontHalf)
    let total = 0
    for (let i = 0; i < data.length; i += 1 | 0) {
      if (data[i] === data2[i]) total += +data[i]
    }
    return total
  }
}
