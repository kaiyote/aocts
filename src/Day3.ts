import { IDay } from './IDay'

export default class Day3 implements IDay<number> {
  private squares: number[] = [1]

  part1 (input: string): number {
    const square = +input
    if (square === 1) return 0
    const sideLength = this.sideLength(square) | 0
    const sideEnd = this.layerEndToSideEnd(square, Math.pow(sideLength, 2)) | 0
    const sideMid = (sideEnd - Math.floor(sideLength / 2)) | 0
    const distToMid = Math.abs(sideMid - square) | 0
    const distFromEdge = Math.floor(sideLength / 2) | 0
    return distToMid + distFromEdge
  }

  part2 (input: string): number {
    const square = +input

    const map = new Map<string, number>()

    let sideLength = 1
    let doXSide = true
    let x = 0
    let y = 0
    let totalAdded = 0
    let val = 0

    while ((val = this.addNextElement(map, x, y)) <= square) {
      totalAdded += 1
      if (doXSide) { // x+
        if (sideLength % 2 !== 0) x += 1
        else x -= 1
        if (totalAdded === sideLength) {
          doXSide = !doXSide
          totalAdded = 0
        }
      } else { // y
        if (sideLength % 2 !== 0) y += 1
        else y -= 1
        if (totalAdded === sideLength) {
          doXSide = !doXSide
          sideLength++
          totalAdded = 0
        }
      }
    }

    return val
  }

  private sideLength (x: number): number {
    while (this.squares[this.squares.length - 1] < x) {
      this.squares.push(Math.pow((this.squares.length + 1) * 2 - 1, 2))
    }

    return Math.sqrt(this.squares.find(n => n >= x)!)
  }

  private layerEndToSideEnd (x: number, layerEnd: number): number {
    let result = layerEnd
    const sideLength = Math.sqrt(layerEnd)
    while (result > x) result -= sideLength - 1
    return result + sideLength - 1
  }

  private addNextElement (map: Map<string, number>, x: number, y: number): number {
    let nextVal = 0

    for (const x1 of [-1, 0, 1]) {
      for (const y1 of [-1, 0, 1]) {
        const coord = `${x + x1}:${y + y1}`
        if (map.has(coord)) nextVal += map.get(coord)!
      }
    }
    if (nextVal === 0) nextVal = 1
    map.set(`${x}:${y}`, nextVal)

    return nextVal
  }
}
