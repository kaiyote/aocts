import { IDay } from './IDay'

export default class Day9 implements IDay<number> {
  part1 (input: string): number {
    const stream = this.prepareInput(input)
    let idx = -1
    // clean out canceled characters
    while ((idx = stream.findIndex(x => x === '!')) !== -1) {
      stream.splice(idx, 2)
    }

    // clean out garbage
    while ((idx = stream.findIndex(x => x === '<')) !== -1) {
      const garbageClose = stream.findIndex((x, i) => x === '>' && i > idx)
      stream.splice(idx, garbageClose - idx + 1)
    }

    return this.computeScore(stream.join(''))
  }

  part2 (input: string): number {
    const stream = this.prepareInput(input)
    let idx = -1
    // clean out canceled characters
    while ((idx = stream.findIndex(x => x === '!')) !== -1) {
      stream.splice(idx, 2)
    }

    let garbageCount = 0
    // clean out garbage
    while ((idx = stream.findIndex(x => x === '<')) !== -1) {
      const garbageClose = stream.findIndex((x, i) => x === '>' && i > idx)
      stream.splice(idx, garbageClose - idx + 1)
      garbageCount += garbageClose - idx - 1
    }

    return garbageCount
  }

  private prepareInput (input: string): string[] {
    return input.trim().split('')
  }

  private computeScore (input: string): number {
    let score = 0
    let depth = 0
    for (const character of input) {
      if (character === '{') {
        depth++
      }
      if (character === '}') {
        score += depth--
      }
    }

    return score
  }
}
