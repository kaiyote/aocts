import { IDay } from './IDay'

export default class Day4 implements IDay<number> {
  part1 (input: string): number {
    return this.prepareInput(input)
      .filter(line => line.every((word, index) => line.lastIndexOf(word) === index))
      .length
  }

  part2 (input: string): number {
    return this.prepareInput(input)
      .map(line => line.map(word => word.split('').sort().join()))
      .filter(line => line.every((word, index) => line.lastIndexOf(word) === index))
      .length
  }

  private prepareInput (input: string): string[][] {
    const data = input.trim().split('\n').map(x => x.trim().split(' '))
    return data
  }
}
