import { Expect, SetupFixture, Test, TestFixture } from 'alsatian'
import Day3 from '../src/Day3'
import { ITestBase } from '../src/IDay'

@TestFixture('Day 3 Tests')
export default class Day3Test implements ITestBase {
  data: string = '347991'
  runner?: Day3

  @SetupFixture
  setupFixture () {
    this.runner = new Day3()
  }

  @Test('Part 1 behaves')
  testPart1 () {
    let result = this.runner!.part1('1')
    Expect(result).toBe(0)
    result = this.runner!.part1('12')
    Expect(result).toBe(3)
    result = this.runner!.part1('23')
    Expect(result).toBe(2)
    result = this.runner!.part1('1024')
    Expect(result).toBe(31)
    result = this.runner!.part1(this.data)
    Expect(result).toBe(480)
  }

  @Test('Part 2 behaves')
  testPart2 () {
    let result = this.runner!.part2('1')
    Expect(result).toBe(2)
    result = this.runner!.part2('20')
    Expect(result).toBe(23)
    result = this.runner!.part2(this.data)
    Expect(result).toBe(349975)
  }
}
