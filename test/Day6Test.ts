import { Expect, SetupFixture, Test, TestFixture } from 'alsatian'
import Day6 from '../src/Day6'
import { ITestBase } from '../src/IDay'

@TestFixture('Day 6 Tests')
export default class Day6Test implements ITestBase {
  runner?: Day6

  @SetupFixture
  setupFixture () {
    this.runner = new Day6()
  }

  @Test('Part 1 behaves')
  testPart1 () {
    let result = this.runner!.part1('0 2 7 0')
    Expect(result).toBe(5)
    result = this.runner!.part1(this.data)
    Expect(result).toBe(3156)
  }

  @Test('Part 2 behaves')
  testPart2 () {
    let result = this.runner!.part2('0 2 7 0')
    Expect(result).toBe(4)
    result = this.runner!.part2(this.data)
    Expect(result).toBe(1610)
  }

  // tslint:disable-next-line:member-ordering max-line-length
  data: string = '2	8	8	5	4	2	3	1	5	5	1	2	15	13	5	14'
}
