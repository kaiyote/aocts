import { Expect, SetupFixture, Test, TestFixture } from 'alsatian'
import Day10 from '../src/Day10'
import { ITestBase } from '../src/IDay'

@TestFixture('Day 10 Tests')
export default class Day10Test implements ITestBase {
  runner?: Day10

  @SetupFixture
  setupFixture () {
    this.runner = new Day10()
  }

  @Test('Part 1 behaves')
  testPart1 () {
    this.runner!.ringLength = 5
    let result = this.runner!.part1('3, 4, 1, 5')
    Expect(result).toBe(12)

    this.runner!.ringLength = 256
    result = this.runner!.part1(this.data)
    Expect(result).toBe(29240)
  }

  @Test('Part 2 behaves')
  testPart2 () {
    let result = this.runner!.part2('')
    Expect(result).toBe('a2582a3a0e66e6e86e3812dcb672a272')
    result = this.runner!.part2('AoC 2017')
    Expect(result).toBe('33efeb34ea91902bb2f59c9920caa6cd')
    result = this.runner!.part2('1,2,3')
    Expect(result).toBe('3efbe78a8d82f29979031a4aa0b16a9d')
    result = this.runner!.part2('1,2,4')
    Expect(result).toBe('63960835bcdc130f0b66d7ff4f6a5a8e')

    result = this.runner!.part2(this.data)
    Expect(result).toBe('4db3799145278dc9f73dcdbc680bd53d')
  }

  // tslint:disable-next-line:member-ordering max-line-length
  data: string = '76,1,88,148,166,217,130,0,128,254,16,2,130,71,255,229'
}
